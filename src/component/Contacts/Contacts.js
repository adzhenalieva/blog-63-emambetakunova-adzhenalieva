import React, {Component} from 'react';
import axios from "../../axios-post";
import Nav from "../Nav/Nav";
import Spinner from "../UI/Spinner/Spinner";

import './Contacts.css';


class Contacts extends Component {

    state = {
        contacts: [],
        loading: true
    };

    componentDidMount() {
        axios.get('/contacts.json').then(response => {
            return response.data;
        }).then(contactData => {
            this.setState({contacts: contactData, loading: false})
        }).catch(error => {
            console.log(error);
        });
    }

    render() {
        let div = (
            <div className="Contacts">
                <Nav/>
                <h3>Contact us</h3>
                <img src={this.state.contacts.image} alt="contacts"/>
                <p><strong>Phone: </strong>{this.state.contacts.phone}</p>
                <p><strong>E-mail: </strong>{this.state.contacts.email}</p>
                <p><strong>Site: </strong><a href="#site">{this.state.contacts.site}</a></p>
            </div>
        );

        if (this.state.loading) {
            div = <Spinner/>
        }

        return (
            <div>
                {div}
            </div>

        );
    }
}

export default Contacts;