import React, {Component} from 'react';
import './Full.css';
import axios from '../../axios-post';
import Spinner from "../UI/Spinner/Spinner";

class FullPost extends Component {
    state = {
        post: null,
        id: null
    };

    goToEdit = (id) => {
        this.props.history.push({
            pathname: '/posts/' + id + '/edit'
        });
    };

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);
        const post = {};
        let idArray = [];
        let id;

        for (let param of query.entries()) {
            post[param[0]] = param[1];
            idArray.push(param[0]);
            id = idArray[0];
        }
        this.setState({post, id})
    }

    deletePost = (id) => {
        axios.delete('/posts/' + id + '.json').then(r => {
            console.log(r);
            this.props.history.push({
                pathname: '/'
            });
        });
    };

    getTime = (response) => {
        let datetime1 = response;
        let date = datetime1.split('T')[0];
        let minutes = datetime1.split('T')[1];
        minutes = minutes[0] + minutes[1] + minutes[2] + minutes[3] + minutes[4];
        return date + ' ' + minutes;
    };

    render() {

        return (
            this.state.post || this.state.id ? <div className="FullPost">
                    <p>{this.getTime(this.state.post.date)}</p>
                    <h3>{this.state.post.title}</h3>
                    <p className="FullPostText">{this.state.post.text}</p>
                    <button className="Edit" onClick={() => this.goToEdit(this.state.id)}>Edit</button>
                    <button className="Delete" onClick={() => this.deletePost(this.state.id)}>Delete</button>
                </div>
                : <Spinner/>
        );
    }
}

export default FullPost;