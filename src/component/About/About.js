import React, {Component} from 'react';
import axios from "../../axios-post";
import Nav from "../Nav/Nav";
import Spinner from "../UI/Spinner/Spinner";
import "./About.css";

class About extends Component {
    state = {
        about: '',
        loading: true
    };

    componentDidMount() {
        axios.get('/about.json').then(response => {
            return response.data;
        }).then(aboutData => {
            let about = [];
            about.push(aboutData.text, aboutData.image);
            this.setState({about, loading: false})
        }).catch(error => {
            console.log(error);
        });
    }

    render() {
        let div = (
            <div className="About">
                <Nav/>
                <h3>About us</h3>
                <img className="AboutImage" src={this.state.about[1]} alt="about"/>
                <p>{this.state.about[0]}</p>
            </div>
        );

        if (this.state.loading) {
            div = <Spinner/>
        }
        return (
            <div>
                {div}
            </div>
        );
    }
}

export default About;