import React, {Component, Fragment} from 'react';
import axios from '../../axios-post';

import PostForm from "../PostForm/PostForm";
import Nav from "../Nav/Nav";

import './PostAdd.css';


class PostAdd extends Component {

    addPost = post => {
        axios.post('/posts.json', post).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <Nav/>
                <h3 className="PostAddTitle">Add new post</h3>
                <PostForm onSubmit={this.addPost}/>
            </Fragment>
        );
    }
}

export default PostAdd;