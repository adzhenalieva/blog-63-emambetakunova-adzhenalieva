import React, {Component} from 'react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './PostForm.css';


class PostAdd extends Component {


    constructor(props) {
        super(props);
        if (props.posts) {
            this.state = {...props.posts};
        } else {
            this.state = {
                posts: {
                    title: '',
                    text: '',
                    date: new Date()
                }

            };
        }
    }

    valueChanged = (event) => {
        let posts = {...this.state.posts};
        let name = event.target.name;
        posts[name] = event.target.value;
        this.setState({posts});
    };

    sendData = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state})
    };

    render() {

        return (
            <div className="PostAdd">
                <form onSubmit={this.sendData}>
                    <p>Title</p>
                    <input className="PostAddInput" name="title" onChange={this.valueChanged}
                           value={this.state.posts.title}
                           type="text"/>
                    <p>Description</p>
                    <textarea className="PostAddInput" name="text" value={this.state.posts.text}
                              onChange={this.valueChanged} cols="30" rows="10"/>
                    <button className="Save">Save</button>
                </form>
            </div>
        );
    }
}

export default PostAdd;