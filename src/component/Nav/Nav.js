import React, {Component} from 'react';
import './Nav.css';
import {NavLink} from 'react-router-dom';

class Nav extends Component {

    render() {
        return (
            <nav className="Nav">
                <NavLink className="NavLink" to="/">Home</NavLink>
                <NavLink className="NavLink" to="/posts/add">Add</NavLink>
                <NavLink className="NavLink" to="/about">About</NavLink>
                <NavLink className="NavLink" to="/contacts">Contacts</NavLink>
            </nav>
        );
    }
}

export default Nav;