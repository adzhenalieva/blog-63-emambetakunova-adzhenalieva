import React, {Component} from 'react';
import axios from '../../axios-post';
import PostForm from "../PostForm/PostForm";
import Spinner from "../UI/Spinner/Spinner";


class PostEdit extends Component {

    state = {
        posts: null
    };

    getUrl = () => {
        const id = this.props.match.params.id;
        return 'posts/' + id + '.json'
    };

    componentDidMount() {
        axios.get(this.getUrl()).then(response => {
            this.setState({posts: response.data})
        })
    }

    editPost = posts => {
        axios.put(this.getUrl(), posts).then(() => {
            this.props.history.replace('/');
        })
    };

    render() {
        let form = <PostForm onSubmit={this.editPost} posts={this.state.posts}/>;

        if (!this.state.posts) {
            form = <Spinner />
        }
        return (
            <div>
                <h2>Edit form</h2>
                {form}
            </div>
        );
    }
}

export default PostEdit;