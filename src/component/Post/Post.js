import React from 'react';
import './Post.css';

const Post = props => {
    return (
        <div className="Post">
            <p>{props.date}</p>
            <h3>{props.title}</h3>
            <button className="ReadMore" onClick={props.readMore}>Read more</button>
        </div>
    );
};

export default Post;