import React, {Component} from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Home from "./container/Home/Home";
import FullPost from "./component/FullPost/FullPost";
import About from "./component/About/About";
import Contacts from "./component/Contacts/Contacts";
import PostAdd from "./component/PostAdd/PostAdd";
import PostEdit from "./component/PostEdit/PostEdit";

import './App.css';





class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/about" component={About}/>
                        <Route path="/contacts" component={Contacts}/>
                        <Route path="/posts/add" component={PostAdd}/>
                        <Route path="/posts/:id/edit" component={PostEdit}/>
                        <Route path="/posts/:id" component={FullPost}/>

                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
