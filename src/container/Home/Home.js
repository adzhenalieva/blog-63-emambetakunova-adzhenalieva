import React, {Component} from 'react';
import './Home.css';
import Post from "../../component/Post/Post";
import axios from '../../axios-post';
import Nav from "../../component/Nav/Nav";
import Spinner from "../../component/Contacts/Contacts";

class Home extends Component {
    state = {
        posts: [],
        postOne: [],
        id: null,
        loading: false
    };

    componentDidMount() {
        axios.get('/posts.json').then(response => {
            return response.data;

        }).then(post => {
            let posts = [];
            for (let key in post) {
                let postOne = post[key].posts;
                postOne.id = key;
                posts.push(postOne);
            }

            this.setState({posts, loading: true});
        }).catch(error => {
            console.log(error);
        });

    }


    giveStateToFullPost = () => {
        const queryParams = [];
        queryParams.push(this.state.id);
        for (let i in this.state.postOne) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.postOne[i]));
        }

        const queryString = queryParams.join('&');
        this.props.history.push({
            pathname: '/posts/:id',
            search: '?' + queryString
        });
    };

    getTime = (response) => {
        let datetime1 = response;
        let date = datetime1.split('T')[0];
        let minutes = datetime1.split('T')[1];
        minutes = minutes[0] + minutes[1] + minutes[2] + minutes[3] + minutes[4];
        return date + ' ' + minutes;
    };


    goToFullPost = (id) => {
        axios.get('/posts/' + id + '.json').then(response => {
            let postOne = response.data.posts;
            this.setState({
                postOne, id
            });
            this.setState({postOne});
            this.giveStateToFullPost();
        }).catch(error => {
            console.log(error);
        });

    };

    render() {
        let divForm = (
            <div className="Home">
                <Nav/>
                {this.state.posts.map((post) =>
                    <Post
                        key={post.id}
                        date={this.getTime(post.date)}
                        title={post.title}
                        readMore={() => this.goToFullPost(post.id)}
                    />
                )}
            </div>
        );

        if (!this.state.loading) {
            divForm = <Spinner/>
        }
        return (
            <div className="HomeWrapper">
                {divForm}
            </div>
        );
    }
}

export default Home;